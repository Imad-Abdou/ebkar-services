# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Ebkar Services1",
			"color": "green",
			"icon": "octicon octicon-mortar-board",
			"type": "module",
			"label": _("Ebkar Services")
		}
	]
