# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "ebkar_services"
app_title = "Ebkar Services1"
app_publisher = "ebkar technology"
app_description = "test app"
app_icon = "octicon octicon-mortar-board"
app_color = "green"
app_email = "sale@ebkar.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/ebkar_services/css/ebkar_services.css"
# app_include_js = "/assets/ebkar_services/js/ebkar_services.js"

# include js, css files in header of web template
# web_include_css = "/assets/ebkar_services/css/ebkar_services.css"
# web_include_js = "/assets/ebkar_services/js/ebkar_services.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "ebkar_services.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "ebkar_services.install.before_install"
# after_install = "ebkar_services.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "ebkar_services.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"ebkar_services.tasks.all"
# 	],
# 	"daily": [
# 		"ebkar_services.tasks.daily"
# 	],
# 	"hourly": [
# 		"ebkar_services.tasks.hourly"
# 	],
# 	"weekly": [
# 		"ebkar_services.tasks.weekly"
# 	]
# 	"monthly": [
# 		"ebkar_services.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "ebkar_services.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "ebkar_services.event.get_events"
# }

